const express = require ("express");

const app = express()

const port = 3000

const users = [
		{
			"username":"johndoe",
			"password":"johndoe1234"
		},
		{
			"username":"janedoe",
			"password":"jane1234"
		}
	]

app.use(express.json())

app.use(express.urlencoded({extended:true}))

app.get("/home", (req,res) =>{
	res.send("Welcome to the home page")
})


app.get("/users", (req, res)=>{
	res.send(users)
})

app.delete("/delete-user",(req, res) =>{
	let message 
	console.log(users)
	if(users.length !== 0){
		for(let i = 0; i<users.length; i++){
			if(req.body.username === users[i].username){
				message = `User ${req.body.username} has been deleted`;
				users.splice(i, 1)
				console.log(users)
				break;

			}if(req.body.username === '' || undefined ){
				message =`User does not exist`
			}else{
				message =`No users found`
		}	
	  }
	}
	res.send(message)
})








if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app