const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for registration
router.post("/register", userController.registration);

// Route for authentication/login
router.post("/login", userController.login);

// Route for retrieving user details
router.get("/details", userController.userDetails);

// Route for setting role as admin
router.patch("/setAdmin/:userId", auth.verify, userController.setAdmin);

// Route for creating order/add to cart
router.post("/checkout", auth.verify, userController.createOrder);

// Route for retrieving authenticated user's order
router.get("/retrieveOrder", auth.verify, userController.getUserOrder);

// Route for retrieving all orders by admin
router.get("/orders", auth.verify, userController.getAllOrders);


module.exports = router;
