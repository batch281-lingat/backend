// ARRAY TRAVERSAL

let grades = [98.5, 94.3, 89.2, 90.1]
let computerBrands= ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

// let mixedArr = [12, 'Asus', null, undefined] // This is not recommended

let tasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
]

// Creating an array with values from variables
let city1 = "Tokyo"
let city2 = "Manila"
let city3 = "Mumbai"


let cities = [city1, city2, city3]

console.log(tasks)
console.log(cities)

// Array length property
console.log("Array length")
console.log(tasks.length)
console.log(cities.length)

let fullName = "Kenneth Lingat"
console.log(fullName.length)

tasks.length = tasks.length -1
console.log(tasks.length)
console.log(tasks)

cities.length--
console.log(cities)

fullName.length = fullName.length - 1;
console.log(fullName.length)

// Adding a number to lengthen the size of the array
let theBeatles = ["John", "Paul", "Ringo", "George"]
theBeatles.length++
console.log(theBeatles)

// Accessing elements of an array
console.log(grades[0])
console.log(computerBrands[3])
console.log(grades[20])

let lakersLegends =["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
console.log(lakersLegends[1])
console.log(lakersLegends[3])

let currentLaker = lakersLegends[2]
console.log("accessing array using variables")
console.log(currentLaker)

// Reassigning values in array
console.log("Array before reassignment")
console.log(lakersLegends)
lakersLegends[2] = "Pau Gasol"
console.log("Arrayt after reassignment")
console.log(lakersLegends)

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex])

// Directly access the expression

console.log(bullsLegends[bullsLegends.length -1 ])



// Adding items into the Array
console.log("Adding items into the Array")
let newArr = [];
console.log(newArr[0])

newArr[0] = "Cloud Strife"
console.log(newArr)

// Looping through array
for(let index = 0; index < computerBrands.length; index++){
	console.log(computerBrands[index])
}

// Checks the element if it's divisible by 5

let numArr = [5, 12, 30, 46, 40]

for (let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	}
	else{
		console.log(numArr[index] + " is not divisible by 5")	
	}
}

// Multidimensional Array
/*
  2 x 2 Two dimensional Array
  let twoDim =[[elem1, elem2],[elem3, elem4]]
  				 0 		1 		0 		1
					0 				1
	twoDim[0][0]; - to access elem1
	twoDim[1][0]; - to access elem3

*/	
let twoDim =[['Kenzo', 'Alonzo'],['Bella', 'Aizaac']]
console.log(twoDim[0][0])
console.log(twoDim[1][0])
console.log(twoDim[0][1])
console.log(twoDim[1][1])

