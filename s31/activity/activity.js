// 1. What directive is used by Node.js in loading the modules it needs?
// ANSWER: Require Directive

// 2. What Node.js module contains a method for server creation?
// ANSWER : http module

// 3. What is the method of the http object responsible for creating a server using Node.js?

// ANSWER:  http.createServer()

// 4. What method of the response object allows us to set status codes and content types?
	
// ANSWER : response.writeHead()

// 5. Where will console.log() output its contents when run in Node.js?

// ANSWER: Terminal or in the console of the browser

// 6. What property of the request object contains the address's endpoint?

// ANSWER: request.url
