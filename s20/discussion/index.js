// While loop
/*
	- it evaluates a condition, if returned true , it will execute statements as long as the condition is satisfied. If at first the condition is not saitsifed, no statement will be executed.

		Syntax:
			while(expression/condition){
				statement/s;
			}
*/
console.log("WHILE LOOP")
let count = 5;

while(count !== 0){
	console.log ("While: " + count)
	count--; 
}


let count2 = 1;

while(count2 <= 5){
	console.log ("While: " + count2)
	count2++; 
}

// Do-while loop
/*
	-iterates statements within a number of times based on a condition.However, if the conditio nwas not satisfied at first, one statement will be executed.

	Syntax:
		do{
			statement/s;
		} while(expression/condition);
*/

/* let number = Number(prompt("Give me a number"))

do{
	console.log("Do While: " + number)
	number +=1;
}while (number < 10 )*/
console.log("DO WHILE")
let even = 2;

do{
	console.log(even);
	even +=2;
}while(even <= 10)

// For Loop
/*
	-a looping construct that is more flexible than other loops. 
		It consists of three parts:
		1. Initialization
		2. Expression/condition
		3. Final Expression/ Step Expression

	Syntax:
		for(initialization; expression/condition; finalExpression){
			statement/s;
		}
*/

console.log("For LOOP!")
for(let count = 0; count <= 20; count++){
	console.log(count)
}

let myString = "alex";
console.log(myString.length)

// Accessing elements of a string
console.log(myString[0])
console.log(myString[2])
console.log(myString[3])


console.log("Looping through array index")
for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}

console.log("LOOPING THROUGH VOWELS AND CONSONANT")
let myName = "AlExIs";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3)
	}else {
		console.log(myName[i])
	}
}

// Continue and Break Statements

console.log('Continue and Break Statements')
for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		// Tells the code to continue to the next iteration of the loop
		continue;
	}
	console.log("Continue and Break: " + count)

	if(count > 10){
		// tells the code to terminate/stop the loop even if the condition of the loop defines taht it should execute
		break;
	}
}

// count=10
console.log('FINAL')
let name = "alexandro"
for (let i = 0; i < name.length; i++){
	console.log(name[i]);
	// If the character is equal to 'a', continue to the next iteration
	if(name[i].toLowerCase() === 'a'){
		console.log("continue to the next iteration");
		continue;
	}
	// if current letter is equal to D , stop the loop
	if(name[i] =="d")
		break;
}	