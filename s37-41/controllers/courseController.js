const Course = require("../models/Course")
const auth = require("../auth")

/*module.exports.addCourse = (reqBody) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};
*/


// ACTIVITY s39
/*module.exports.addCourse = (req,res) =>{

	const userData = auth.decode(req.headers.authorization)

	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price: req.body.price
	})

	if(userData.isAdmin == true){
		return newCourse.save().then(result =>{
			res.send(true)
		})
		}else{
			res.send (false)
	}
}
		
*/
module.exports.addCourse = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};


// Retrive all courses
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result =>{
		return result
	})
}

// Retrieve all active courses
module.exports.getAllActive =() =>{
	return Course.find({isActive : true}).then(result =>{
		return result
	})
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result
	})
}

// Updaing a course
module.exports.updateCourse = (reqParams , reqBody) =>{

	// Specify the fields properties of the documents to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error ) =>{

		// Course not updated
		if(error){
			return false
		// Course updated successfully
		}else {
			return true
		}

	})
}

// ACTIVITY s40
/*
module.exports.archiveCourse = (reqParams, reqBody) => {
	
	let archivedCourse = {
		isActive : reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) =>{
		if (error) {
			return false
		}else {
			return true
		}
	})

}*/

// STRETCH GOAL
/*
module.exports.archiveCourse = (reqParams, data) => {
	
	let archivedCourse = {
		isActive : data.course.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) =>{
		if (error) {
			return false
		}else {
			return true
		}
	})

}

*/

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};
