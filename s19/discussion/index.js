// console.log("Hello WORLD!")


// [SECTION] Selection Control Structures
/*
	- it sorts out wheteher the statement/s are to be executed on the condition whether it is true or false
	- two-way (true or false)
	- multi-way selection
*/

// If ... else statement
/*
	Syntax:
		if(condition){
			//statement
		} else if {
			//statement
		}
*/

	// If Statement = executes a statement if a specified condition is true

	let numA = -1;

	if (numA < 0) {
		console.log("The number is less than 0")
	}

	console.log(numA < 0);

	let city = "New York"

	if (city === "New York") {
		console.log("Welcome To New York City!")
	}

	// Else if
		/*
			-executes a statement if the previous conditions are false and if the specified condition is true.
			- the "else if" clause is optional and can be added to capture addtional conditions to change the flow of the program
			*/
	let numB = 1;

	if (numA > 0){
		console.log("Hello")
	} else if(numB > 0){
		console.log("World")
	}


	city = "Tokyo"

	if(city === "New York"){
		console.log("Welcome to New York City")
	}else if (city === "Tokyo"){
		console.log("Welcome to Tokyo!")
	}

	// Else statement
		//  - executes a statement if all of our previous conditions are false.
	
	if (numA > 0){
		console.log("Hello")
	} else if (numB === 0){
		console.log("World")
	} else {
		console.log("Again")
	}
	// parseInt - for Integer
	// // let age = parseInt(prompt("Enter your age: "))
	// if (age <= 18 ){
	// 	console.log("Not Allowed to Drink")
	// } else {
	// 	console.log("Matanda ka na! Shot puno!")
	// } 
	// Mini Activity  (function is for reusability)
	function minHeight(height){
		if (height <= 150 ) {
		console.log("Did not pass the min height requirment")
		} else if (height > 150){
		console.log("Passed the min height requirement")
		}
	}
	minHeight(183);
	minHeight(130)

	let message = "No message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed < 30) {
			return "Not a typhoon"
		} else if(windSpeed <=61 ){
			return 'Tropical Depression Detected'
		} else if (windSpeed >=62 && windSpeed <=88){
			return 'Tropical storm detected'
		} else if (windSpeed >=89 && windSpeed <=177){
			return 'Severe Tropical Storm Detected'
		} else {
			return 'Typhoon Detected'
		}
	}

	message = determineTyphoonIntensity(70)
	// message = determineTyphoonIntensity(99)
	// message = determineTyphoonIntensity(178)
	console.log(message)

	if (message == "Tropical storm detected"){
		console.warn(message)
	}

	// Truthy and Falsy

	/*
		In JS a truthy value is a value that is considered true when encountered in a boolean context.

		Falsy value:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
	*/	

	// Truthy Examples:

	let word = "true"
	if(word){
		console.log("Truthy");
	};

	if(true) {
		console.log("Truthy");
	};

	if(1) {
		console.log("Truthy");
	};


	// Falsy Examples:
	if(false) {
		console.log("Falsy");
	};

	if(0) {
		console.log("Falsy");
	};

	if(undefined) {
		console.log("Falsy");
	};

	if(null) {
		console.log("Falsy");
	};

	if(-0) {
		console.log("Falsy");
	};

	if(NaN) {
		console.log("Falsy");
	};


// Condition Ternary Operator - for short codes

	/*
	 Ternary operator takes in three oeprands
	  	1. condition.
	  	2. expression to execute if the condition is true/truthy/.
	  	3. expression to execute if the condition is false/falsy.
		
	  Syntax :
	  	(condition) ? ifTrue_expression : isFalse_expression.	
	*/


// Single statement execution

let ternaryResult = (1 < 18) ? "Condition is true" : "Condition is false"

console.log("Result of the ternary operator: "+ ternaryResult)

//  Multiple Statement Execution

let name;

function isOfLegalAge(){
	name = " John"
	return "You are of the legal age limit"
}

function isUnderAge(){
	name = "Jane"
	return "You are under the age limit"
}

/*let yourAge = parseInt(prompt("What is your age?"))
console.log(yourAge)

let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();

console.log("result of ternary operator in function: " + legalAge + ", " + name)*/

// [Section] Switch statement

/*
	Can be used an alternative to if ... else statements where the data to be used in the condition is of an expected input.

	Syntax:
		switch (expression/condition){
			case <value>:
				statement;
				break;
			default:
				statement;
				break;
		}
*/


let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red.")
		break;
	case 'tuesday':
		console.log("The color of the day is orange.")
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.")
		break;
	case 'thursday':
		console.log("The color of the day is green.")
		break;	
	case 'friday':
		console.log("The color of the day is blue.")
		break;
	case 'saturday':
		console.log("The color of the day is indigo.")
		break;
	case 'sunday':
		console.log("The color of the day is violet.")
		break;
	default:
		console.log("Please input a valid day of the week")
		break;		
}
// break; is for ending the loop
// switch - is not a very popular statement in coding


// [Section] Try-Catch-Finally Statement

/*
	-try-catch is commonly used for error handling
	-will still function even if the statement is not complete/ without the finally code block.
*/

function showIntensityAlert(windSpeed){
	try{
		alerto(determineTyphoonIntensity(windSpeed))
	}

	catch (error){
		console.log(typeof error)
		console.log(error)
		console.warn(error.message)
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}

showIntensityAlert(56);
