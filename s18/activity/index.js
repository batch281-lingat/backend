
// PART 1
function addition(num1, num2){
	let sum = num1 + num2
	console.log("Displayed sum of "+num1+ " and " +num2)
	console.log(sum)
}
addition(5, 10)

function subtraction(num1, num2){
	let difference = num1 - num2
	console.log("Displayed difference of "+num1+ " and " +num2)
	console.log(difference)
}
subtraction(20, 5)

// PART 2

function multiplication(num1, num2){
	let multiply = num1 * num2
	console.log("The Product of " +num1+  " and " + num2)
	return multiply
}

let product = multiplication(50, 10)

console.log(product)

function division(num1, num2){
	let divide = num1 / num2
	console.log("The quotient of " +num1+  " and " + num2)
	return divide
}

let quotient = division(50, 10)

console.log(quotient)

// PART 3

function radius(num1){
	let area = 3.1416 * num1**2
	console.log("The result of getting the are of a circle with" +num1+ " radius:")
	return area
}

let circleArea = radius(15)
console.log(circleArea)


// PART 4

function average(num1, num2, num3, num4){
	let totalAverage = (num1 + num2 + num3 + num4) / 4
	console.log("The average of "+ num1+" " + num2+ " " + num3+" and "+ num4)
	return totalAverage
}

let averageVar = average(20, 40, 60, 80)
console.log(averageVar)

// PART 5

function passingScore(num1, num2){
	let passing = 0.75 * num2
	ifPassed = num1 >= passing
	console.log("Is " + num1 + "/" +num2 + " a passing score?")
	return ifPassed
}

let isPassingScore = passingScore(38, 100)
console.log(isPassingScore)
