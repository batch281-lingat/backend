// JSON object
/*
	-JSON stands for JavaScript Object Notation
		Syntax:
		{
			"propertyA" : "valueA"
			"propertyB" : "valueB"
		}
*/

// JSON as objects
/*{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}*/

// JSON Arrays
/*"cities":[
	{ "city" : " Quezon City", "province": "Metro manila", "country" : "Philippines"},	
	{ "city" : " Manila City", "province": "Metro manila", "country" : "Philippines"},	
	{ "city" : " Makati City", "province": "Metro manila", "country" : "Philippines"}	
] 	*/

// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name,age,breed
/*"dogs": [
	{"name": "Bruno", "age":"2", "breed" :"Labrador" },	
	{"name": "Brownbish", "age":"3", "breed" :"Golden Retriever" },	
	{"name": "Jutsy", "age":"5", "breed" :"Chihuahua" }	
	]
*/

// JSON methods

// Convert Data intro Stringified JSON
let batchesArr = [{ batchname: 'Batch X'}, {batchName: 'Batch Y'}]

// the "stringify" method is used to convert JavaScript objects into a string
console.log('Result from stringify method: ')
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name : "Kenny",
	age : 25,
	address:{
		city: 'Quezon City',
		country: 'Philippines'
	}
})

console.log(data)

// Using Stringify method with variables
// User details 
/*let firstName = prompt('What is your first name?')
let lastName = prompt('What is your Last name?')
let age = prompt('What is your age')
let address = {
	city:prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')

} 

let otherData =JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData)*/

// Mini Activity - Create a JSON that will accept user car details with variables brand,type, year.
/*let brand = prompt ('What is the brand of your car?')
let type = prompt ('What type of car do you have?')
let year = prompt('Please enter manufacture year ')

let carData = JSON.stringify({
	brand : brand,
	type: type,
	year: year
})

console.log(carData)*/

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}] `

console.log('Result from parse method')
console.log(JSON.parse(batchesJSON))